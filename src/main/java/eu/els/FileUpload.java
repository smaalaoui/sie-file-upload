package eu.els;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class FileUpload {

    private static HttpClient client;

    public FileUpload(String username, String password) {
        UsernamePasswordCredentials credentials
                = new UsernamePasswordCredentials(username, password);
        CredentialsProvider provider = new BasicCredentialsProvider();
        provider.setCredentials(AuthScope.ANY, credentials);
        this.client = HttpClientBuilder
                .create()
                .setDefaultCredentialsProvider(provider)
                .build();
    }


    public int put(String url, File file, Map<String, String> parameters) throws IOException {
        HttpPut putRequest = new HttpPut(url);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create()
                .addPart("file", getFileBody(file));
        parameters
                .keySet()
                .stream()
                .forEach((key) -> builder.addTextBody(key, parameters.get(key)));
        putRequest.setEntity(builder.build());
        HttpResponse response = execute(putRequest);
        return response.getStatusLine().getStatusCode();
    }

    private HttpResponse execute(HttpUriRequest request) throws IOException {
        return client.execute(request);
    }

    private FileBody getFileBody(File file) {
        return new FileBody(file, ContentType.DEFAULT_BINARY);
    }

}
