package eu.els;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FileUploadTest {

    private static final String USERNAME = "els";
    private static final String PASSWORD = "bNELCOIt";
    private static final String REMOTE_REPOSITORY = "efl_documents_recette2";

    private FileUpload fileUpload;

    @Before
    public void setUp() {
        this.fileUpload = new FileUpload(USERNAME, PASSWORD);
    }

    @Test
    public void given_zip_file_when_put_then_should_return_200_OK() throws IOException {
        // GIVEN
        String url = "https://efl.data.integration.wuha.io/upload/zip";
        File file = getFile();
        Map<String, String> params = getParameters();

        // WHEN
        int result = this.fileUpload.put(url, file, params);

        // THEN
        assertEquals(200, result);
    }

    private File getFile() {
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource("test_1.zip").getFile());
    }

    private Map<String, String> getParameters() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("repository", REMOTE_REPOSITORY);
        return parameters;
    }

}
